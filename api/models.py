from django.db import models
from django.contrib.auth.models import User as DjangoUser

class Post(models.Model):
    author = models.ForeignKey(DjangoUser, on_delete=models.CASCADE,related_name='posts')
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.author.username} -{self.content} - {self.created_at}"

class Comment(models.Model):
    author = models.ForeignKey(DjangoUser, on_delete=models.CASCADE,related_name='comments')
    post = models.ForeignKey(Post, on_delete=models.CASCADE,related_name='comments')
    content = models.TextField()

    def __str__(self):
        return f"{self.author.username} -{self.content}"
class UserProfile(models.Model):
    followers = models.ManyToManyField(DjangoUser, related_name='following')