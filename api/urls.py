from django.urls import include, path
from rest_framework import routers
from .views import PostViewSet, CommentViewSet, CommentListCreateView,UserViewSet, FollowUser

# Routers provide a way of automatically determining the URL conf.
router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'posts', PostViewSet)
router.register(r'comments', CommentViewSet, basename='comment')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('posts/', include([
        path('<int:post_id>/comments/', CommentListCreateView.as_view(), name='comment-list-create'),
    ])),
    path('users/<int:follower_id>/follow/<int:followee_id>/', FollowUser.as_view(), name='follow-user'),

]
