from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from api.models import Post, Comment


class UserListTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = User.objects.create_user(username='user1', password='password1')
        self.user2 = User.objects.create_user(username='user2', password='password2')

    def test_get_user_list(self):
        # Authenticate a user
        self.client.force_authenticate(user=self.user1)

        # Make a GET request to the endpoint
        response = self.client.get('/api/users/')

        # Check that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Convert the response data to a list of dictionaries
        response_data = list(response.data)

        # Check that the response contains the correct number of users
        self.assertEqual(len(response_data), 2)

        # Define expected data before printing
        expected_data = [
            {'id': self.user1.id, 'username': 'user1'},
            {'id': self.user2.id, 'username': 'user2'},]
class PostDetailTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.post = Post.objects.create(author=self.user, content='Test post content')

    def test_get_post_detail(self):
        # Make a GET request to the endpoint to retrieve details of a specific post
        response = self.client.get(f'/api/posts/{self.post.id}/')

        # Check that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check that the response contains the expected data
        self.assertEqual(response.data['id'], self.post.id)
        self.assertEqual(response.data['author'], self.user.id)
        self.assertEqual(response.data['content'], 'Test post content')

        # Assuming you have a 'comments' field in your serializer
        self.assertIn('comments', response.data)

        # Check that the 'comments' field contains the last three comments
        last_three_comments = self.post.comments.all().order_by('-created_at')[:3]
        self.assertEqual(len(response.data['comments']), len(last_three_comments))

        # Check that the 'comments' field contains the correct content
        for i in range(len(last_three_comments)):
            self.assertEqual(response.data['comments'][i]['content'], last_three_comments[i].content)

    def test_get_post_detail_invalid_id(self):
        # Make a GET request to the endpoint with an invalid post ID
        response = self.client.get('/api/posts/999/')

        # Check that the response status code is 404 Not Found
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class PostCommentsTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.post = Post.objects.create(author=self.user, content='Test post content')
        self.comment1 = Comment.objects.create(author=self.user, post=self.post, content='Comment 1')
        self.comment2 = Comment.objects.create(author=self.user, post=self.post, content='Comment 2')
        self.comment3 = Comment.objects.create(author=self.user, post=self.post, content='Comment 3')

    def test_get_post_comments(self):
        # Make a GET request to the endpoint to retrieve all comments for a specific post
        response = self.client.get(f'/api/posts/{self.post.id}/comments/')

        # Check that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check that the response contains the expected number of comments
        self.assertEqual(len(response.data), 3)

        # Check that the response contains the correct content for each comment
        self.assertEqual(response.data[0]['content'], 'Comment 1')
        self.assertEqual(response.data[1]['content'], 'Comment 2')
        self.assertEqual(response.data[2]['content'], 'Comment 3')

    def test_get_post_comments_invalid_id(self):
        # Make a GET request to the endpoint with an invalid post ID
        response = self.client.get('/api/posts/999/comments/')

        # Check that the response status code is 404 Not Found
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)