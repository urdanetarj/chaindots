# api/views.py
from django.contrib.auth.models import User
from django.db import transaction
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from .models import Post, Comment, UserProfile
from .serializers import UserSerializer, PostSerializer, CommentSerializer
from rest_framework.response import Response
from rest_framework import generics, viewsets, status
from rest_framework import serializers

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class FollowUser(generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        follower_id = self.kwargs['follower_id']
        followee_id = self.kwargs['followee_id']

        follower_profile = get_object_or_404(UserProfile, user__id=follower_id)
        followee_profile = get_object_or_404(UserProfile, user__id=followee_id)

        if follower_profile.user == followee_profile.user:
            return Response({"detail": "Cannot follow yourself"}, status=status.HTTP_400_BAD_REQUEST)

        if followee_profile.user in follower_profile.following.all():
            return Response({"detail": "Already following this user"}, status=status.HTTP_400_BAD_REQUEST)

        follower_profile.following.add(followee_profile.user)

        return Response({"detail": f"User {follower_id} is now following user {followee_id}"}, status=status.HTTP_200_OK)
class PostList(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        queryset = Post.objects.all().order_by('-created_at')
        author_id = self.request.query_params.get('author_id')
        from_date = self.request.query_params.get('from_date')
        to_date = self.request.query_params.get('to_date')

        if author_id:
            queryset = queryset.filter(author__id=author_id)  # Update this line
        if from_date:
            queryset = queryset.filter(created_at__gte=from_date)
        if to_date:
            queryset = queryset.filter(created_at__lte=to_date)

        return queryset

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

class CommentListCreateView(generics.ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        post_id = self.kwargs['post_id']
        return Comment.objects.filter(post_id=post_id)

    def perform_create(self, serializer):
        post_id = self.kwargs['post_id']
        serializer.save(author=self.request.user, post_id=post_id)

class UserProfileSerializer(serializers.ModelSerializer):
    total_posts = serializers.SerializerMethodField()
    total_comments = serializers.SerializerMethodField()
    followers_count = serializers.SerializerMethodField()
    following_count = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['user', 'total_posts', 'total_comments', 'followers_count', 'following_count']

    def get_total_posts(self, obj):
        return obj.posts.count()

    def get_total_comments(self, obj):
        return obj.comments.count()

    def get_followers_count(self, obj):
        if hasattr(obj, 'followers'):
            return obj.followers.count()
        return 0

    def get_following_count(self, obj):
        if hasattr(obj.user, 'following'):
            return obj.user.following.count()
        return 0