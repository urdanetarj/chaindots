# api/serializers.py
from rest_framework import serializers
from django.contrib.auth.models import User

from .models import Post, Comment

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'author', 'content','created_at']
       # depth=1

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id', 'author', 'post', 'content']
        depth = 1 # Indica la profundidad para incluir la información completa del autor

class UserSerializer(serializers.ModelSerializer):
    posts = PostSerializer(many=True, read_only=True)
    total_posts = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True, read_only=True)
    total_comments = serializers.SerializerMethodField()
   # followers_count = serializers.SerializerMethodField()
   # following_count = serializers.SerializerMethodField()
    class Meta:
       model = User
       fields = ['id', 'username', 'email','posts', 'total_posts','comments','total_comments']
    def get_total_posts(self, obj):
        return obj.posts.count()
    def get_total_comments(self, obj):
        return obj.comments.count()


