# chaindots

## Comenzando :rocket:

_El proyecto tiene 1 branchs activas correspondientes a cada ambiente:_

* [master](https://gitlab.com/urdanetarj/chaindots) - Branch de producción.

## Pre-requisitos :clipboard:

_Requisitos necesarios para el funcionamiento de la aplicación_

- Python 3.8
- Django 4.2
- git

## Construido con :hammer_and_wrench:

_Detalle de las soluciones que se utilizaron en el proyecto_

* Python
* Sqlite

## Documentación :page_facing_up:

### Despliegue :package:
* [Acceder](https://gitlab.com/urdanetarj/chaindots/-/blob/main/DEPLOY.md?ref_type=heads) - Detalle para desplegar el proyecto en cada ambiente.
