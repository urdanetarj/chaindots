---
### Local

1. Clonar proyecto:
```
git clone https://gitlab.com/urdanetarj/chaindots.git /opt/docker/chaindots
```
2. 
```
pipenv –python 3.8  
```
3. 
```
pipenv python manage.py makemigrations (crear la migracion)
```
4.
```
pipenv run python manage.py migrate (correr migraciones)
```
5. 
```
pipenv run python manage.py createsuperuser  (crear el superusuario)
```
6. 
```
pipenv run pip list  (nos da una lista de los paquetes instalados)
```
7.
```
pipenv run python manage.py runserver  (levantar servidor local)
```
